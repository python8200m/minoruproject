import re

def removeAcentos(texto):
    new = texto.lower()
    new = re.sub(r'[àáâãäå]', 'a', new)
    new = re.sub(r'[èéêë]', 'e', new)
    new = re.sub(r'[ìíîï]', 'i', new)
    new = re.sub(r'[òóôõö]', 'o', new)
    new = re.sub(r'[ùúûü]', 'u', new)
    return new

nomeArquivo = 'Aula05\\faroeste.txt'
#arquivo = open(nomeArquivo,'r',encoding='utf-8').read()
with open(nomeArquivo,'r',encoding='utf-8') as arquivo:
    arquivo = removeAcentos(arquivo.read())

    qtdVogalA = arquivo.count('a')
    qtdVogalE =arquivo.count('e')
    qtdVogalI =arquivo.count('i')
    qtdVogalO =arquivo.count('o')
    qtdVogalU =arquivo.count('u')

    print(f'''Ao analisar o arquivo {nomeArquivo}, temos as seguintes quantidade de vogais:
    A:{qtdVogalA}
    E:{qtdVogalE}
    I:{qtdVogalI}
    O:{qtdVogalO}
    U:{qtdVogalU}
    Totalizando:{qtdVogalA+qtdVogalE+qtdVogalI+qtdVogalO+qtdVogalU}
    ''')
