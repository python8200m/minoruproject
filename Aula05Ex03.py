import csv

listaCadastro = []
with open('Aula05\\Ex02.csv','r') as csvfile:
    for linhas in  csv.reader(csvfile):
        for conteudo in linhas:
            listaCadastro.append(conteudo)
    
    while True:
        print('Bem vindo ao menu de busca de cadastro, temos as seguintes opções para buscar:')
        i = 1
        for valor in (listaCadastro[0].split(';')):
            print(f'Digite {i} - {valor}: ')
            i += 1
        opcao = int(input('Digite por qual deseja realizar uma busca. '))
        valorBusca = input('Qual valor você quer buscar? ')

        for linha in listaCadastro:
            if(valorBusca == linha.split(';')[opcao-1]):
                print(f'Registro encontado, seguem os dados:\n{listaCadastro[0]}\n{linha}')
        
        opcao = int(input('Digite 1 para buscar novamente ou 2 para sair. '))

        if(opcao == 2):
            break