# Crie um programa que pergunte para o usuario um numero de pessoas a participarem de um sorteio (0-20),
#  e o numero de pessoas a serem sorteadas, e depois sorteie esse numero de pessoas da lista.

# O programa deverá pegar o numero de pessoas a participar laeatoriamente desta lista:

# lista = ["Joao", "Maria", "Tiago", "Amanda", "Emanuele", "Caio", "Suzana", "Miguel", 
# "Rosangela", "Rian", "Lucimar", "Ulisses", "Leonardo", "Kaique", "Bruno", "Raquel", 
# "Benedito", "Tereza", "Valmir", "Joaquim"]
import random as rd

listaGeralParticipantes = ["Joao", "Maria", "Tiago", "Amanda", "Emanuele", "Caio", "Suzana", "Miguel", 
"Rosangela", "Rian", "Lucimar", "Ulisses", "Leonardo", "Kaique", "Bruno", "Raquel", 
"Benedito", "Tereza", "Valmir", "Joaquim"]

listaCandidatos = []
listaGanhadores = []

print(f'''Hoje vai acontecer um sorteio grande e esses são os participantes: {listaGeralParticipantes}''')
qtdCandidatos = int(input('Digite quantas pessoas você quer que participem do sorteio? Escolha entre 1 e 20 pessoas. '))

while qtdCandidatos != 0 :
    candidato = rd.choice(listaGeralParticipantes)
    listaCandidatos.append(candidato)
    qtdCandidatos -= 1

qtdSorteados = int(input(f'Digite quantas pessoas você quer que participem do sorteio?. Escolha entre 1 e {len(listaCandidatos)}. '))

while qtdSorteados != 0 :
    sorteado = rd.choice(listaCandidatos)
    listaGanhadores.append(sorteado)
    qtdSorteados -= 1

print(f'Os ganhadores foram:{listaGanhadores}')