import lib

#Lista de variáveis
listaPessoa = []

while True:
    nome = input('Digite um nome: ')
    idade = int(input('Digite uma idade: '))

    listaPessoa = lib.adicionaLista(listaPessoa, nome,idade)

    if idade < 0:
        print('Idade inválida')

    else:
        opcao = int(input('Digite 1 para incluir mais pessoas na lista ou 2 parar. '))

        if opcao == 2 :
            break

listaPessoa = lib.geraExtratoIdade(listaPessoa)

opcao = int(input('Digite 1 exibir o nome das pessoas de cada grupo  ou 2 para sair. '))

if opcao == 1 :
    lib.geraExtratoNomes(listaPessoa[0],listaPessoa[1],listaPessoa[2])