def calculaSoma(n1, n2):
    resultado = n1 + n2
    return(resultado)


def calculaSubstracao(n1, n2):
    resultado = n1 - n2
    return(resultado)


def calculaDivisao(n1, n2):
    resultado = n1 / n2    
    return(resultado)


def calculaMultiplicacao(n1, n2):
    resultado = n1 * n2
    return(resultado)


operacao = ''
n1 = ''
n2 = ''

while True:
    try:
        n1 = float(input('Digite um número: '))
        break
    except Exception:
        print('O que foi digitado não é um número')

while True:
    try:
        n2 = float(input('Digite um segundo número: '))
        break
    except Exception:
        print('O que foi digitado não é um número')   

while True:
    try:
        opcao = int(input('''Digite qual operação você quer realizar com esses números:
1 - Soma
2 - Subtração
3 - Divisão
4 - Multiplicação
'''))
    except Exception:
        print('O que foi digitado não é um número')

    if opcao == 1:
        resultado = calculaSoma(n1,n2)
        operacao = 'Soma'
        break
    elif opcao == 2:
        resultado = calculaSubstracao(n1,n2)
        operacao = 'Subtração'
        break
    elif opcao == 3:
        try:
            resultado = calculaDivisao(n1,n2)
            operacao = 'Divisão'
        except ZeroDivisionError:
            print('Com esses números aconteceu divisão por 0, escolha outra operação ')
        else:
            break
    elif opcao == 4:
        resultado = calculaMultiplicacao(n1,n2)
        operacao = 'Multiplicação'
        break
    else: 
        print('Operação inválida')

print(f'''Os números digitados foram: {n1} e {n2}
A operação escolhida foi:{operacao}
Com isso, o resultado é: {resultado:.2f}
''')




