'''
Receber um nome e imprimir uma saudação
Chamada da função saudacao('Lalo')
Saída: 'Olá Lalo! Tudo bem com você?
'''

def saudacao(nome):
    print(f'''
    Chamada da função saudacao(\'{nome}\')
    Saída: \'Olá {nome}! Tudo bem com você?\'
    ''')

nome = input('Digite um nome: ')
saudacao(nome)