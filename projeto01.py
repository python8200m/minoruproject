#Imports
import sqlite3

#Funções
def geraLog(login,evento,parametro):
    conexao = sqlite3.connect('segBanco.db')
    cursor = conexao.cursor()

    sqlString = f'''
    INSERT INTO log(login,evento,parametro)
    SELECT '{login}','{evento}','{parametro}'
    '''
    cursor.execute(sqlString)
    conexao.commit()
    cursor.close()


def bloqueiaUsuario(login):
    geraLog(login,'bloqueiaUsuario','')
    conexao = sqlite3.connect('segBanco.db')
    cursor = conexao.cursor()

    sqlString = f'''
    UPDATE User
    SET flgAtivo = 0
    WHERE login = '{login}'
    '''

    cursor.execute(sqlString)
    conexao.commit()
    cursor.close()

    print(f'O usuário {login} foi bloqueado')


def realizaLogin(login,senha):
    geraLog(login,'realizaLogin','')
    conexao = sqlite3.connect('segBanco.db')
    cursor = conexao.cursor()
    
    sqlString = f'''
    SELECT 
        *
    FROM User
    where login = '{login}'
    and senha = {senha}
    ;
    '''

    cursor.execute(sqlString)
    usuario = cursor.fetchone()
    
    cursor.close()
    return usuario


def validaLoginUsuario(login):
    conexao = sqlite3.connect('segBanco.db')
    cursor = conexao.cursor()
    
    sqlString = f'''
    SELECT 
        *
    FROM User
    where login = '{login}'
    and flgAtivo = 1;
    '''

    cursor.execute(sqlString)
    usuario = cursor.fetchone()
    cursor.close()

    if usuario is None:
        geraLog(login,'ValidaLoginFalha','')
        return False
    else:
        return True


def validaLoginBloqueado(login):
    conexao = sqlite3.connect('segBanco.db')
    cursor = conexao.cursor()
    
    sqlString = f'''
    SELECT 
        *
    FROM User
    where login = '{login}'
    and flgAtivo = 0;
    '''

    cursor.execute(sqlString)
    usuario = cursor.fetchone()
    cursor.close()

    if usuario is None:
        return False
    else:
        geraLog(login,'TentativaLoginBloqueado','')
        return True

    
def adicionaUsuario(nome,senha,tipoUsuario):
    geraLog(login,'adicionaUsuario',tipoUsuario)
    conexao = sqlite3.connect('segBanco.db')
    cursor = conexao.cursor()

    sqlString = f'''
    INSERT INTO User(login,senha,tipoUsuario,saldo,flgAtivo)
    SELECT '{nome}',{senha},'{tipoUsuario}',0.00,1
    '''

    cursor.execute(sqlString)
    conexao.commit()
    cursor.close()

    print('Usuário Adicionado com sucesso')


def consultaSaldo(id):
    geraLog(login,'consultaSaldo','')
    conexao = sqlite3.connect('segBanco.db')
    cursor = conexao.cursor()
    
    sqlString = f'''
    SELECT 
        saldo
    FROM User
    where id = {id}
    ;
    '''
    
    cursor.execute(sqlString)
    usuario = cursor.fetchone()
    print(f'Seu saldo é de:{usuario[0]:.2f}')
    cursor.close()


def realizaDeposito(id,valorDepositado):
    geraLog(login,f'realizaDeposito',valorDepositado)
    conexao = sqlite3.connect('segBanco.db')
    cursor = conexao.cursor()
    
    sqlString = f'''
    UPDATE User
    SET saldo = saldo + {valorDepositado}
    where id = {id}
    ;
    '''

    cursor.execute(sqlString)
    conexao.commit()
    
    print(f'O valor de {valorDepositado} foi depositado')
    cursor.close()


def realizaSaque(id,valorSaque):
    geraLog(login,'realizaSaque',valorSaque)
    conexao = sqlite3.connect('segBanco.db')
    cursor = conexao.cursor()
    
    sqlString = f'''
    UPDATE User
    SET saldo = saldo - {valorSaque}
    where id = {id}
    ;
    '''
    cursor.execute(sqlString)
    conexao.commit()
    
    print(f'O valor de {valorSaque} foi sacado')
    cursor.close()


def redefineSenha(id,login,novaSenha):
    geraLog(login,'redefineSenha','')
    conexao = sqlite3.connect('segBanco.db')
    cursor = conexao.cursor()

    sqlString = f'''
    UPDATE User
    SET senha = {novaSenha}
        ,flgAtivo = 1
    WHERE id = {id}
    '''

    cursor.execute(sqlString)
    conexao.commit()
    cursor.close()

    print('Senha alterada com sucesso')


def menuInicial(usuario):
    geraLog(login,'menuInicial','')
    if usuario[3] == 'comum':
        while True:
            print('Segue as opções:')
            print('1 - Consulta de saldo')
            print('2 - Realizar deposito')
            print('3 - Realizar Saque')
            print('4 - Sair')
            try:
                opcao = int(input())
            except ValueError:
                print('A senha só aceita números')

            if opcao == 1:
                consultaSaldo(usuario[0])
            elif opcao == 2:
                while True:
                    try:
                        valorDepositado = float(input('Digite um valor a ser depositado. '))
                        break
                    except ValueError:
                        print('O valor só aceita números')
                realizaDeposito(usuario[0],valorDepositado)
            elif opcao == 3:
                while True:
                    try:
                        valorSaque = float(input('Digite um valor a ser Sacado. '))
                        break
                    except ValueError:
                        print('O valor só aceita números')
                realizaSaque(usuario[0],valorSaque)
            elif opcao == 4:
                break
            else:
                print('Opção Inválida')

    else:
        while True:
            print('Menu de administrador, digite uma das opções abaixo')
            print('1 - Criar um novo usuário comum')
            print('2 - Redefinir senha de um usuário')
            print('3 - Sair')
            while True:
                try:
                    opcao = int(input())
                    break
                except ValueError:
                    print('A senha só aceita números')
            if opcao == 1:
                menuAdicionaUsuario()
            elif opcao == 2:
                menuResetSenha()
            elif opcao == 3:
                break
            else:
                print('Opção Inválida')


def menuAdicionaUsuario():
    geraLog(login,'menuAdicionaUsuario','')
    while True:
        try:
            login = input('Digite um login para um novo usuário: ')
            if login.isalpha() == False:
                raise(ValueError)
            break
        except ValueError:
            print('O Login só aceita letras.')

    while True:
        try:
            senha = int(input('Digite sua senha numérica: '))
            break
        except ValueError:
            print('A senha só aceita números')

    adicionaUsuario(login,senha,'comum')


def menuResetSenha():
    geraLog(login,'menuResetSenha','')
    conexao = sqlite3.connect('segBanco.db')
    cursor = conexao.cursor()
    
    sqlString = f'''
    SELECT 
        *
    FROM User
    where tipoUsuario = 'comum'
    ;
    '''

    cursor.execute(sqlString)
    listaUsuario = cursor.fetchall()
    cursor.close()

    print('Segue a lista de usuários, escolha um para redifinir a senha')
    
    while True:
        for usuario in listaUsuario:
            if(usuario[5]==0):
                print(f'{usuario[0]} - {usuario[1]} (Usuário Bloqueado)')
            else:
                print(f'{usuario[0]} - {usuario[1]}')

        try:
            opcao = int(input(''))
            
            usuarioValido = False
            for usuario in listaUsuario:
                if(usuario[0]==opcao):
                    usuarioValido = True
                    break
            if usuarioValido == True:
                break
            else:
                print('Usuário não encontrado, digite novamente')
        except ValueError:
            print('A opção só aceita números')

    while True:
        try:
            senha = int(input('Digite a nova senha: '))
            break
        except ValueError:
            print('A senha só aceita números')

    redefineSenha(opcao,usuario[1],senha)


#Main
usuario = []
print('Bem vindo ao MinoBank')
quantidadeTentativas = 0
while True:
    try:
        login = input('Digite seu login: ')
        if login.isalpha() == False:
            raise(ValueError)
        
        if quantidadeTentativas < 2:
            if validaLoginUsuario(login) ==  False:
                if validaLoginBloqueado(login) == False:
                    quantidadeTentativas += 1
                    print(f'Usuário não existe, se digitar mais {3-quantidadeTentativas} erros, o programa será encerrado')
                else:
                    quantidadeTentativas = 3
                    print(f'O usuário {login} está bloqueado, procure o gerente da conta')
                    break
            else:
                break
        else:
            break
    except ValueError:
        print('O Login só aceita letras.')

if quantidadeTentativas < 2:
    while True:
        try:
            senha = int(input('Digite sua senha numérica: '))
            usuario = realizaLogin(login,senha)

            if usuario is None:
                quantidadeTentativas += 1
                
                if quantidadeTentativas > 2:
                    bloqueiaUsuario(login)
                    break
                else:
                    print(f'Senha inválida, se digitar mais {3-quantidadeTentativas} errados, o usuário vai ser bloqueado')
            else:
                menuInicial(usuario)
                break
        except ValueError:
            print('A senha só aceita números')

geraLog(login,'Saindo do programa','')
print('Programa está sendo encerrado')

#Fim
'''
Leon
986521

Tiago
125678
'''