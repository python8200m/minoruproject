# 1) Crie uma classe que represente um ônibus. O ônibus deverá conter os seguintes atributos:
# capacidade total
# capacidade atual
# placa
# modelo
# movimento

# Os comportamentos esperados para um Ônibus são:
# Embarcar
# Desembarcar
# Acelerar
# Frear

# Lembre-se que a capacidade total do ônibus é de 10 pessoas - não será possível admitir super-
# lotação. Além disso, quando o ônibus ficar vazio, não será permitido efetuar o desembarque
# de pessoas.

class Onibus:
    def __init__(self, capacidadeTotal = 10,capacidadeAtual = 0,movimento = "Não"):
        self.__capacidadeTotal = capacidadeTotal
        self.__capacidadeAtual = capacidadeAtual
        self.__movimento = movimento

    def embarcar(self):
        if self.__movimento == "Sim":
            print("O ônibus está em movimento, não pode subir ninguém")
        elif self.__capacidadeAtual + 1 > self.__capacidadeTotal:
            print('Máximo de pessoas já foi atingido, essa pessoa não subiu')
        else: 
            self.__capacidadeAtual += 1
            if self.__capacidadeAtual < self.__capacidadeTotal:
                print(f'Um passageiro subiu, estamos com {self.__capacidadeAtual}, ainda é possível subir, temos {self.__capacidadeTotal - self.__capacidadeAtual} lugares')
            else:
                print(f'Um passageiro subiu, estamos com {self.__capacidadeAtual}, chegamos na capacidade máxima')

    def desembarcar(self):
        if self.__movimento == "Sim":
            print("O ônibus está em movimento, não pode subir ninguém")
        elif self.__capacidadeAtual - 1 < 0 :
            print('Não tem ninguém no ônibus')
        else: 
            self.__capacidadeAtual -= 1
            if self.__capacidadeAtual > 0:
                print(f'Desceu um passageiro, estamos com {self.__capacidadeAtual} passageiros, ainda temos {self.__capacidadeTotal - self.__capacidadeAtual} lugares')
            else:
                print('Agora que esse passageiro desceu, o ônibus está vazio')
            

            
    def acelerar(self):
        if self.__movimento == 'Sim':
            print("O ônibus já está em movimento, não é possível acelerar mais por causa do limite de velocidade")
        else:
            self.__movimento = 'Sim'
            print("O ônibus começou a andar")
            if self.__capacidadeAtual == self.__capacidadeTotal:
                print('Estamos com a capacidade máxima, cuidado ao acelerar')
        
    def frear(self):
        if self.__movimento == 'Não':
            print("O ônibus já está parado")
        else:
            self.__movimento = 'Não'
            print("O ônibus parou")
            if self.__capacidadeAtual == self.__capacidadeTotal:
                print('Estamos com a capacidade máxima, cuidado para ninguém cair')
                        
   

bus = Onibus()
bus.frear()
bus.acelerar()
bus.embarcar()
bus.frear()
bus.desembarcar()
bus.embarcar()#1
bus.embarcar()#2
bus.acelerar()
bus.embarcar()#3
bus.frear()
bus.embarcar()#3
bus.embarcar()#4
bus.embarcar()#5
bus.embarcar()#6
bus.embarcar()#7
bus.embarcar()#8
bus.embarcar()#9
bus.embarcar()#10
bus.embarcar()#11#Erro
bus.acelerar()
bus.frear()
bus.desembarcar()#1
bus.desembarcar()#2
bus.desembarcar()#3
bus.desembarcar()#4
bus.desembarcar()#5
bus.desembarcar()#6
bus.desembarcar()#7
bus.desembarcar()#8
bus.desembarcar()#9
bus.desembarcar()#10