import re
from xml.dom.minidom import TypeInfo

print("Preencha as informações solicitadas a seguir")

inputErrado = [1,1,1]
cpf = ''
idade = ''

while inputErrado[0] == 1 or inputErrado[1] == 1 or inputErrado[2] == 1:
    try:
        if (inputErrado[0] == 1):
            nome = input("Nome: ")

        if (inputErrado[1] == 1):
            cpf = int(input("Cpf: "))

        if (inputErrado[2] == 1):
            idade = int(input("Idade: "))

        if(len(re.findall('[0-9]+', nome)) > 0):
            raise TypeError()

    except ValueError:
        if cpf == '':
            print('Não recebemos um valor válido para o cpf')
        elif type(cpf) is not int:
            print('Apenas números são permitidos para o cpf')
        
        if idade == '':
            print('Não recebemos um valor válido para a idade')
        elif type(idade) is not int:
            print('Apenas números são permitidos para a idade')

    except Exception:
        print('Números não são permitidos para o nome')
    
    finally:
        if len(re.findall('[0-9]+', nome)) == 0:
            inputErrado[0] = 0

        if len(re.findall('^0-9', str(cpf))) == 0 and str(cpf) != '':
            inputErrado[1] = 0

        if len(re.findall('^0-9+', str(idade))) == 0 and str(idade) != '':
            inputErrado[2] = 0

        if inputErrado[0] == 1 or inputErrado[1] == 1 or inputErrado[2] == 1:
            print('Como tivemos problemas com a entrada de dados, vamos pegar os que tiveram problemas somente')
        else:
            print('Todas as informações estavam corretas')

print("-----------------------------")
print("Confirmação de cadastro:")
print(f"Nome: {nome}")
print(f"CPF: {cpf}")
print(f"Idade: {idade}")
print("-----------------------------")
