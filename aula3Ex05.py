def calculaAreaRetangulo(base,lado):
    area = base * lado
    return(area)


def calculaQuantidadeLatas(area):
    resultado = area / 3
    return (resultado)

base = int(input("Digite quantos metros tem a base do retângulo: "))
lado = int(input("Digite quantos metros tem o lado do retângulo: "))

area = calculaAreaRetangulo(base,lado)
quantidadeLatas = calculaQuantidadeLatas(area)

print(f'''Com a base de {base}m e com o lado de {lado}m, temos {area}m de área
Seriam necessárias {quantidadeLatas:.2f} latas de tinta para pintar essa área''')