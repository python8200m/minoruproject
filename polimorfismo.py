class Cliente:
    
    def __init__(self,nome):
        self.__carrinho = []
        self.__nome = nome
        self.__total = 0.00

    def adicionaItemCarrinho(self,item):
        self.__carrinho.append(item)
    
    def calculaConta(self):
        self.__total = len(self.__carrinho) * 4.99
        return self.__total


class ClienteVip(Cliente):
    def __init__(self, nome):
        super().__init__(nome)
        
    def calculaConta(self):
        self.__total = (len(self.__carrinho ) * 4.99 * 0.90)  #Da 10% de desconto
        return self.__total

cli = Cliente("Minoru")
cli.adicionaItemCarrinho('Arroz')
cli.adicionaItemCarrinho('Feijão')
cli.adicionaItemCarrinho('1Kg de Banana')
print(f'O valor da sua compra é de: {cli.calculaConta()} reais')

cli2 = ClienteVip("Marcos")
cli2.adicionaItemCarrinho('Arroz')
cli2.adicionaItemCarrinho('Feijão')
cli2.adicionaItemCarrinho('1Kg de Banana')
print(f'O valor da sua compra é de: {cli2.calculaConta()} reais')