import sqlite3
from time import sleep
from random import randint

def carregaEstoque(opcao):  
    conexao = sqlite3.connect('lanchonete.db')
    cursor = conexao.cursor()

    sqlString = f'''
        SELECT 
            * 
        FROM Produto 
        WHERE tipoProduto = '{opcao}';
    '''
    cursor.execute(sqlString)
    idLinha = 1
    lista = []
    for linha in cursor.fetchall():
        lista.append((idLinha,linha))
        idLinha += 1
    cursor.close()
    return lista


def apresentaMenu(opcao):
    print(f'Menu de {opcao}s:')
    print('----------------')

    if opcao =='Bebida':
        for x,y in listaBebida:
            print(f'{x} - {y[1]} - R$ {y[3]:.2f}')
            
    elif opcao =='Comida':
        for x,y in listaComida:
            print(f'{x} - {y[1]} - R$ {y[3]:.2f}')
    
    print('----------------')


def atualizaEstoque(n1,n2):
    conexao = sqlite3.connect('lanchonete.db')
    cursor = conexao.cursor()

    if n1 != -1:
        #Atualiza Bebida
        sqlString = f'''
            UPDATE Produto 
            SET quantidade = quantidade - 1
            WHERE id = {listaBebida[n1][1][0]};
        '''
        try:
            cursor.execute(sqlString)

        except Exception as error:
            print(f'Query Executada:{sqlString}')
            print(error)

    if n2 != -1:
        #Atualiza Comida
        sqlString = f'''
            UPDATE Produto 
            SET quantidade = quantidade - 1
            WHERE id = {listaComida[n2][1][0]};
        '''
        try:
            cursor.execute(sqlString)
        except Exception as error:
            print(f'Query Executada:{sqlString}')
            print(error)

    conexao.commit()    
    cursor.close()


def validaEstoque(lista):
    qtdEstoque = 0
    for n1 in lista:
        qtdEstoque += n1[1][4]
    return qtdEstoque


print('Bem vindo ao simulador de uma lanchonete')
listaBebida = []
listaComida = []
listaPedidos = []

qtdCliente =1
flgSemBebida = False
flgSemComida = False
while True:
    listaBebida = carregaEstoque('Bebida')
    listaComida = carregaEstoque('Comida')

    print(f'Cliente Nº {qtdCliente}, seja bem vindo')
    print(f'Vamos pegar primeiro sua bebida')
    apresentaMenu('Bebida')

    while True:
        try:
            print('Digite a bebida desejada: ')
            n1 = randint(0,len(listaBebida) - 1)
            print(f'O Valor escolhido foi:{n1}')
            #print(listaBebida[n1])
            #print(listaBebida[n1][1][4])
            
            if flgSemBebida == False:
                if listaBebida[n1][1][4] == 0:
                    print(f'A bebida {listaBebida[n1][1][1]} não está mais disponível, escolha outra opção')
                    if validaEstoque(listaBebida) == 0:
                        flgSemBebida = True
                        break
                else:
                    print(f'Já vamos trazer sua {listaBebida[n1][1][1]}.')
                    break
            else:
                n1 = -1
                print('Acabou a bebida, vamos tentar alguma comida')
                break
        except ValueError:
            print('O que foi digitado não é um número, digite novamente')
    
    print(f'Agora vamos apresentar as opções de comida, escolha a opção que deseja: ')
    apresentaMenu('Comida')
    while True:
        try:
            print('Digite a bebida desejada: ')
            n2 = randint(0,len(listaComida)-1)
            print(f'O Valor escolhido foi:{n2}')
            #print(listaBebida[n1])
            #print(listaBebida[n1][1][4])
            
            if flgSemComida == False:
                if listaComida[n2][1][4] == 0:
                    print(f'A bebida {listaComida[n2][1][1]} não está mais disponível, escolha outra opção')
                    if validaEstoque(listaComida) == 0:
                        flgSemComida = True
                        break
                else:
                    print(f'Já vamos seu {listaComida[n2][1][1]}.')
                    break
            else:
                n2 = -1
                print('Acabou a Comida, vamos tentar alguma comida')
                break
        except ValueError:
            print('O que foi digitado não é um número, digite novamente')
    qtdCliente += 1

    atualizaEstoque(n1,n2)

    if flgSemBebida == True and flgSemComida == True:
        print('Desculpe, acabou todo o estoque')
        break

    sleep(5)