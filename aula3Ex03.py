#Banana 3,50
#Melancia 7,50
#Morango 5

#1 - Ver cesta
#2 - Adicionar Frutas
#3 - CheckOut
#4 - Sair

#1 - Ver cesta
#2 - Adicionar Frutas
#3 - Sair

#1 - Banana
#2 - Melancia
#3 - Morango

def somaCarrinho(listaFrutas):
    valorTotal = 0
    if len(listaFrutas) ==0 :
        print("O carrinho está vazio")
        print()
    else:
        print(f"Segue a lista que já foi adicionada ao carrinho")
        for fruta in (listaFrutas):
            valorTotal += fruta[1]
        print(f"O valor total é de:{valorTotal:.2f}")
        print()


def mostraCarrinho(listaFrutas):
    if len(listaFrutas)==0 :
            print("O carrinho está vazio")
            print()
    else:
        print(f"Segue a lista que já foi adicionada ao carrinho")
        for fruta in (listaFrutas):
            print(fruta[0])
        print()


def adicionaItemCarrinho(listaFrutas,opcaoFruta):
    if opcaoFruta == 1:
        listaFrutas.append(("Banana",3.50))
    elif opcaoFruta == 2:
        listaFrutas.append(("Melancia",7.50))
    elif opcaoFruta == 3:
        listaFrutas.append(("Morango",5))
    else: 
        print("Opção Inválida")
    return(listaFrutas)


opcaoMenu = 0
opcaoFruta = 0
valorTotal = 0
listaFrutas = []

while opcaoMenu != 4:
    opcaoMenu = int(input(f"""Menu Principal:
Quitanda:
1: Ver Certa
2: Adicionar Frutas
3: CheckOut
4: Sair
"""))
    if opcaoMenu == 1:
        mostraCarrinho(listaFrutas)
    elif opcaoMenu == 2:
        print()
        opcaoFruta = int(input(f"""Menu de Frutas:
Digite a opção desejada, escolhendo a fruta:
1: Banana
2: Melancia
3: Morango
"""))
        listaFrutas = adicionaItemCarrinho(listaFrutas,opcaoFruta)
        print()
    elif opcaoMenu == 3:    
        somaCarrinho(listaFrutas)