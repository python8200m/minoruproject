n1 = input("Digite o primeiro número:")
n2 = input("Digite o segundo número:")

soma = int(n1) + int(n2)
diff = int(n1) - int(n2)

print(f"Soma: {n1} + {n2} = {soma}")
print(f"Diferença: {n1} - {n2} = {diff}")