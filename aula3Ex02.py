def calculaSoma(n1, n2):
    resultado = n1 + n2
    return(resultado)
def calculaSubstracao(n1, n2):
    resultado = n1 - n2
    return(resultado)
def calculaDivisao(n1, n2):
    resultado = n1 / n2
    return(resultado)
def calculaMultiplicacao(n1, n2):
    resultado = n1 * n2
    return(resultado)

resultado = 0
operacao = ''
n1 = int(input('Digite um número: '))
n2 = int(input('Digite um segundo número: '))
opcao = int(input('''Digite qual operação você quer realizar com esses números:
1 - Soma
2 - Subtração
3 - Divisão
4 - Multiplicação
'''))
if opcao == 1:
    resultado = calculaSoma(n1,n2)
    operacao = 'Soma'
elif opcao == 2:
    resultado = calculaSubstracao(n1,n2)
    operacao = 'Subtração'
elif opcao == 3:
    resultado = calculaDivisao(n1,n2)
    operacao = 'Divisão'
elif opcao == 4:
    resultado = calculaDivisao(n1,n2)
    operacao = 'Multiplicação'
else: print('Operação inválida')


print(f'''Os números digitados foram: {n1} e {n2}
A operação escolhida foi:{operacao}
Com isso, o resultado é: {resultado:.2f}
''')