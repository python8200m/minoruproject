anoNascimento = int(input("Digite o ano que você nasceu? "))
classificacao = ""
if anoNascimento < 1964:
    classificacao = "Baby Bomber"
elif anoNascimento >= 1965 and anoNascimento <= 1979:
    classificacao = "X"
elif anoNascimento >= 1980 and anoNascimento <= 1994:
    classificacao = "Y"
else: 
    classificacao = "Z"

print(f"Você que nasceu no ano {anoNascimento} é da geração {classificacao}")

#Baby Bomber <= 1964
#X - 1965 - 1979
#Y - 1980 - 1994
#Z - 1995 - Atual


