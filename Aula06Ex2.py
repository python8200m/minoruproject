# 2) Implemente um programa que represente uma fila. O contexto do programa é uma
# agência de banco. Cada cliente ao chegar deverá respeitar a seguinte regra: o primeiro
# a chegar deverá ser o primeiro a sair. Você poderá representar pessoas na fila a par-
# tir de números os quais representam a idade. A sua fila deverá conter os seguintes
# comportamentos:

# • Adicionar pessoa na fila: adicionar uma pessoa na fila.
# • Atender Fila: atender a pessoa respeitando a ordem de chegada
# • Dar prioridade: colocar uma pessoa maior de 65 anos como o primeiro da fila

class Agencia:
    def __init__(self):
        self.__filaAtendimento = []
        self.__normal = 0
        self.__priori = 0
    
    def geraTicketAtendimento(self,tipoFila):#2 normal, 1 prioritário          
        if tipoFila==1:
            self.__normal += 1           
            ticket = f'{tipoFila}.{self.__normal}'
        else:
            self.__priori += 1
            ticket = f'{tipoFila}.{self.__priori}'

        self.__filaAtendimento.append(ticket)
        print(f'Você recebeu a ficha:{ticket}')
   
    def retornaAtendimento(self):
            if len(self.__filaAtendimento) == 0:
                print('Não tem ninguém na fila')
            else:
                self.__filaAtendimento.sort()
                print('PRÓXIMO ATENDIMENTO:' + self.__filaAtendimento[0])
                del self.__filaAtendimento[0]
            
    def zeraFila(self,tipoFila=0):#1 normal, 0 prioritário
        if tipoFila == 1:
            self.__priori = 0
            print('A fila priorizada foi zerada')
        elif tipoFila == 2:
            self.__normal = 0
            print('A fila normal foi zerada')
        else:
            self.__priori = 0
            self.__normal = 0
            print('Todas as fila foram zeradas')

    
agencia = Agencia()

agencia.geraTicketAtendimento(2)#Normal
agencia.geraTicketAtendimento(2)#Normal
agencia.geraTicketAtendimento(1)#Prioritário
agencia.geraTicketAtendimento(2)#Normal
agencia.retornaAtendimento()
agencia.retornaAtendimento()
agencia.geraTicketAtendimento(1)#Prioritário
agencia.geraTicketAtendimento(2)#Normal
agencia.geraTicketAtendimento(1)#Prioritário
agencia.geraTicketAtendimento(1)#Prioritário
agencia.retornaAtendimento()
agencia.retornaAtendimento()
agencia.retornaAtendimento()
agencia.geraTicketAtendimento(2)#Normal
agencia.retornaAtendimento()
agencia.retornaAtendimento()
agencia.retornaAtendimento()
agencia.retornaAtendimento()
agencia.retornaAtendimento()
agencia.zeraFila()