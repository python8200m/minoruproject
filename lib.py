def adicionaLista(listaPessoa, nome, idade):
    listaPessoa.append({nome:idade})
    return(listaPessoa)


def geraExtratoIdade(listaPessoa):
    g1 = 0
    g2 = 0
    g3 = 0
    ListaPessoaG1 = []
    ListaPessoaG2 = []
    ListaPessoaG3 = []
    ListaPessoasGeral = []
    
    for pessoa in listaPessoa:
        for nome,idade in pessoa.items():
            if(idade < 18):
                g1 += 1
                ListaPessoaG1.append(nome)
            elif(idade < 60):
                g2 += 1
                ListaPessoaG2.append(nome)
            else: 
                g3 += 1
                ListaPessoaG3.append(nome)
    ListaPessoasGeral.append(ListaPessoaG1)
    ListaPessoasGeral.append(ListaPessoaG2)
    ListaPessoasGeral.append(ListaPessoaG3)
    print(f'''
    Extrado das idades:
    0-17 anos: {g1}
    18-59 anos: {g2}
    +60 anos: {g3}
    ''')
    return(ListaPessoasGeral)

def geraExtratoNomes(lista1,lista2,lista3):
    print(f'''
    Pessoas de 0 a 17 anos:{lista1}
    Pessoas de 18 a 59 anos:{lista2}
    Pessoas com 60 ou mais anos:{lista3}
    ''')