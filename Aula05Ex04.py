import csv

listaCadastro = []
linha = 0

with open('Aula05\\Ex02.csv','r') as csvfile:
    for linhas in  csv.reader(csvfile):
        for conteudo in linhas:
            listaCadastro.append(conteudo)


for conteudo in listaCadastro:
    if linha == 0:
        print(f'Lista de Cadastro:')
    else:
        print(f'Linha {linha}: {conteudo}')
    linha += 1

opcao = int(input(f'Digite a linha que você quer remover: '))

if opcao > 0:
    listaCadastro.pop(opcao)

    with open('Aula05\\Ex02.csv','w', newline="") as csvfile:
        writer = csv.writer(csvfile, delimiter=';')

        for conteudo in listaCadastro:
            writer.writerow([conteudo])
else:
    print('Opção inválida')

