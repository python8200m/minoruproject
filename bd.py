import sqlite3

conexao = sqlite3.connect('segBanco.db')
cursor = conexao.cursor()

sqlString = '''
CREATE TABLE IF NOT EXISTS User(
    id  integer primary key autoincrement
    ,login text
    ,senha text
    ,tipoUsuario text
    ,saldo real
    ,flgAtivo int
);
'''
cursor.execute(sqlString)
conexao.commit()

sqlString = '''
CREATE TABLE IF NOT EXISTS Log(
    id  integer primary key autoincrement
    ,login text
    ,evento text
    ,parametro text
    ,dataEvento datetime default current_timestamp
);
'''
cursor.execute(sqlString)
conexao.commit()

sqlString = '''
INSERT INTO User(login,senha,tipoUsuario,saldo,flgAtivo)
SELECT 'Tiago','125678','admin',0,1
UNION ALL SELECT 'Leon','986521','comum',200.20,1
UNION ALL SELECT 'Minoru','123','comum',3500.56,1
'''

cursor.execute(sqlString)
conexao.commit()

cursor.close()